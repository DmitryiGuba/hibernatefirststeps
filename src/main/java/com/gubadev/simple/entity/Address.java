package com.gubadev.simple.entity;


import javax.persistence.*;

@Embeddable
public class Address {
    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;

    @Column(name = "street")
    private String street;

    @Column(name = "building")
    private int building;

    @Column(name = "floor")
    private int floor;

    @Column(name = "flat")
    private int flat;

    public Address() {
    }

    public Address(String country, String city, String street, int building, int floor, int flat) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.building = building;
        this.floor = floor;
        this.flat = flat;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getBuilding() {
        return building;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getFlat() {
        return flat;
    }

    public void setFlat(int flat) {
        this.flat = flat;
    }
}
