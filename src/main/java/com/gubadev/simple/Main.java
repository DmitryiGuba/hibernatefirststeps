package com.gubadev.simple;

import com.gubadev.simple.dao.impl.AutorDAO;
import com.gubadev.simple.entity.Address;
import com.gubadev.simple.entity.Autor;
import com.gubadev.simple.entity.Book;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        SessionFactory sessionFactory = null;
        AutorDAO autorDAO = new AutorDAO();

        final StandardServiceRegistry receiveRegistry = new StandardServiceRegistryBuilder().configure().build();

        try {

            MetadataSources meta = new MetadataSources(receiveRegistry);
            Metadata md = meta.buildMetadata();
            sessionFactory = md.buildSessionFactory();
        } catch (Throwable th) {
            System.out.print("Problem with factory creation");
            StandardServiceRegistryBuilder.destroy(receiveRegistry);
            throw new ExceptionInInitializerError(th);
        }

        autorDAO.setSessionFactory(sessionFactory);

        Address firstAddress = new Address();
        firstAddress.setCountry("Russia");
        firstAddress.setCity("Saint-Petersburg");
        firstAddress.setStreet("Nevskiy avenue");
        firstAddress.setBuilding(15);
        firstAddress.setFloor(2);
        firstAddress.setFlat(25);


        Address secondAddress = new Address();
        secondAddress.setCountry("England");
        secondAddress.setCity("London");
        secondAddress.setStreet("Backer");
        secondAddress.setBuilding(1);
        secondAddress.setFloor(1);
        secondAddress.setFlat(1);


        Address thirdAddress = new Address();
        thirdAddress.setCountry("Russia");
        thirdAddress.setCity("Moscow");
        thirdAddress.setStreet("Arbat");
        thirdAddress.setBuilding(27);
        thirdAddress.setFloor(5);
        thirdAddress.setFlat(576);

        Address fourthAddress = new Address();
        fourthAddress.setCountry("Russia");
        fourthAddress.setCity("Saint-Petersburg");
        fourthAddress.setStreet("Ligovskiy avenue");
        fourthAddress.setBuilding(57);
        fourthAddress.setFloor(15);
        fourthAddress.setFlat(48);

        Address fifthAddress = new Address();
        fifthAddress.setCountry("German");
        fifthAddress.setCity("Berlin");
        fifthAddress.setStreet("Bundestag");
        fifthAddress.setBuilding(127);
        fifthAddress.setFloor(8);
        fifthAddress.setFlat(187);

        Book firstFirstBook = new Book();
        Book firstSecondBook = new Book();
        Book firstThirdBook = new Book();
        Book firstFourthBook = new Book();
        Book firstFifthBook = new Book();
        Book secondFirstBook = new Book();
        Book secondSecondBook = new Book();
        Book secondThirdBook = new Book();
        Book secondFourthBook = new Book();
        Book secondFifthBook = new Book();
        Book thirdFirstBook = new Book();
        Book thirdSecondBook = new Book();
        Book thirdThirdhBook = new Book();
        Book thirdFourthBook = new Book();
        Book thirdFifthBook = new Book();
        Book fourthFirstBook = new Book();
        Book fourthSecondBook = new Book();
        Book fourthThirdBook = new Book();
        Book fourthFourthBook = new Book();
        Book fourthFifthBook = new Book();
        Book fifthFirstBook = new Book();
        Book fifthSecondBook = new Book();
        Book fifthThirdBook = new Book();
        Book fifthFourthBook = new Book();
        Book fifthFifthBook = new Book();


        Autor firstAutor = new Autor();
        Autor secondAutor = new Autor();
        Autor thirdAutor = new Autor();
        Autor fourthAutor = new Autor();
        Autor fifthAutor = new Autor();


        firstFirstBook.setTitle("Alexander 1");
        firstFirstBook.setDescription("About Alexander 1");
        firstFirstBook.setPages(100);
        firstFirstBook.setAutor(firstAutor);

        firstSecondBook.setTitle("Alexander 2");
        firstSecondBook.setDescription("About Alexander 2");
        firstSecondBook.setPages(200);
        firstSecondBook.setAutor(firstAutor);

        firstThirdBook.setTitle("Alexander 3");
        firstThirdBook.setDescription("About Alexander 3");
        firstThirdBook.setPages(300);
        firstThirdBook.setAutor(firstAutor);

        firstFourthBook.setTitle("Alexander 4");
        firstFourthBook.setDescription("About Alexander 4");
        firstFourthBook.setPages(400);
        firstFourthBook.setAutor(firstAutor);

        firstFifthBook.setTitle("Alexander 5");
        firstFifthBook.setDescription("About Alexander 5");
        firstFifthBook.setPages(500);
        firstFifthBook.setAutor(firstAutor);


        secondFirstBook.setTitle("Ser 1");
        secondFirstBook.setDescription("About Ser 1");
        secondFirstBook.setPages(100);
        secondFirstBook.setAutor(secondAutor);

        secondSecondBook.setTitle("Ser 2");
        secondSecondBook.setDescription("About Ser 2");
        secondSecondBook.setPages(200);
        secondSecondBook.setAutor(secondAutor);

        secondThirdBook.setTitle("Ser 3");
        secondThirdBook.setDescription("About Ser 3");
        secondThirdBook.setPages(300);
        secondThirdBook.setAutor(secondAutor);

        secondFourthBook.setTitle("Ser 4");
        secondFourthBook.setDescription("About Ser 4");
        secondFourthBook.setPages(400);
        secondFourthBook.setAutor(secondAutor);

        secondFifthBook.setTitle("Ser 5");
        secondFifthBook.setDescription("About Ser 5");
        secondFifthBook.setPages(500);
        secondFifthBook.setAutor(secondAutor);


        thirdFirstBook.setTitle("Yriy 1");
        thirdFirstBook.setDescription("About Yriy 1");
        thirdFirstBook.setPages(100);
        thirdFirstBook.setAutor(thirdAutor);

        thirdSecondBook.setTitle("Yriy 2");
        thirdSecondBook.setDescription("About Yriy 2");
        thirdSecondBook.setPages(200);
        thirdSecondBook.setAutor(thirdAutor);

        thirdThirdhBook.setTitle("Yriy 3");
        thirdThirdhBook.setDescription("About Yriy 3");
        thirdThirdhBook.setPages(300);
        thirdThirdhBook.setAutor(thirdAutor);

        thirdFourthBook.setTitle("Yriy 4");
        thirdFourthBook.setDescription("About Yriy 4");
        thirdFourthBook.setPages(400);
        thirdFourthBook.setAutor(thirdAutor);

        thirdFifthBook.setTitle("Yriy 5");
        thirdFifthBook.setDescription("About Yriy 5");
        thirdFifthBook.setPages(500);
        thirdFifthBook.setAutor(thirdAutor);


        fourthFirstBook.setTitle("Petr 1");
        fourthFirstBook.setDescription("About Petr 1");
        fourthFirstBook.setPages(100);
        fourthFirstBook.setAutor(fourthAutor);

        fourthSecondBook.setTitle("Petr 2");
        fourthSecondBook.setDescription("About Petr 2");
        fourthSecondBook.setPages(200);
        fourthSecondBook.setAutor(fourthAutor);

        fourthThirdBook.setTitle("Petr 3");
        fourthThirdBook.setDescription("About Petr 3");
        fourthThirdBook.setPages(300);
        fourthThirdBook.setAutor(fourthAutor);

        fourthFourthBook.setTitle("Petr 4");
        fourthFourthBook.setDescription("About Petr 4");
        fourthFourthBook.setPages(400);
        fourthFourthBook.setAutor(fourthAutor);

        fourthFifthBook.setTitle("Petr 5");
        fourthFifthBook.setDescription("About Petr 5");
        fourthFifthBook.setPages(500);
        fourthFifthBook.setAutor(fourthAutor);


        fifthFirstBook.setTitle("Angela 1");
        fifthFirstBook.setDescription("About Angela 1");
        fifthFirstBook.setPages(100);
        fifthFirstBook.setAutor(fourthAutor);

        fifthSecondBook.setTitle("Angela 2");
        fifthSecondBook.setDescription("About Angela 2");
        fifthSecondBook.setPages(200);
        fifthSecondBook.setAutor(fourthAutor);

        fifthThirdBook.setTitle("Angela 3");
        fifthThirdBook.setDescription("About Angela 3");
        fifthThirdBook.setPages(300);
        fifthThirdBook.setAutor(fourthAutor);

        fifthFourthBook.setTitle("Angela 4");
        fifthFourthBook.setDescription("About Angela 4");
        fifthFourthBook.setPages(400);
        fifthFourthBook.setAutor(fourthAutor);

        fifthFifthBook.setTitle("Angela 5");
        fifthFifthBook.setDescription("About Angela 5");
        fifthFifthBook.setPages(500);
        fifthFifthBook.setAutor(fourthAutor);


        Set<Book> firstBooks = new HashSet<>();
        firstBooks.add(firstFifthBook);
        firstBooks.add(firstFifthBook);
        firstBooks.add(firstFifthBook);
        firstBooks.add(firstFifthBook);
        firstBooks.add(firstFifthBook);

        Set<Book> secondBooks = new HashSet<>();
        secondBooks.add(secondFirstBook);
        secondBooks.add(secondSecondBook);
        secondBooks.add(secondFourthBook);
        secondBooks.add(secondThirdBook);
        secondBooks.add(secondFifthBook);

        Set<Book> thirdBooks = new HashSet<>();
        thirdBooks.add(thirdFirstBook);
        thirdBooks.add(thirdSecondBook);
        thirdBooks.add(thirdFourthBook);
        thirdBooks.add(thirdThirdhBook);
        thirdBooks.add(thirdFifthBook);

        Set<Book> fourthBooks = new HashSet<>();
        fourthBooks.add(fourthFifthBook);
        fourthBooks.add(fourthFifthBook);
        fourthBooks.add(fourthFifthBook);
        fourthBooks.add(fourthFifthBook);
        fourthBooks.add(fourthFifthBook);

        Set<Book> fifthBooks = new HashSet<>();
        fifthBooks.add(fifthFirstBook);
        fifthBooks.add(fifthSecondBook);
        fifthBooks.add(fifthThirdBook);
        fifthBooks.add(fifthFourthBook);
        fifthBooks.add(fifthFifthBook);


        firstAutor.setFirstName("Alexander");
        firstAutor.setLastName("Nevskiy");
        firstAutor.setAge(250);
        firstAutor.setAddress(firstAddress);
        firstAutor.setBooks(firstBooks);

        secondAutor.setFirstName("Ser");
        secondAutor.setLastName("Charlz");
        secondAutor.setAge(550);
        secondAutor.setAddress(secondAddress);
        secondAutor.setBooks(secondBooks);

        thirdAutor.setFirstName("Yriy");
        thirdAutor.setLastName("Dolgorukiy");
        thirdAutor.setAge(750);
        thirdAutor.setAddress(thirdAddress);
        thirdAutor.setBooks(thirdBooks);

        fourthAutor.setFirstName("Petr");
        fourthAutor.setLastName("First");
        fourthAutor.setAge(350);
        fourthAutor.setAddress(fourthAddress);
        fourthAutor.setBooks(fourthBooks);

        fifthAutor.setFirstName("Angela");
        fifthAutor.setLastName("Merkel");
        fifthAutor.setAge(55);
        fifthAutor.setAddress(fifthAddress);
        fifthAutor.setBooks(fifthBooks);


        autorDAO.add(firstAutor);
        autorDAO.add(secondAutor);
        autorDAO.add(thirdAutor);
        autorDAO.add(fourthAutor);
        autorDAO.add(fifthAutor);

        List<Autor> autors = autorDAO.findAll();
        for (Autor autor : autors) {
            System.out.println(autor);
        }

        autorDAO.delete(secondAutor);
        autorDAO.delete(fifthAutor);

        autors = autorDAO.findAll();
        for (Autor autor : autors) {
            System.out.println(autor);
        }

    }
}
