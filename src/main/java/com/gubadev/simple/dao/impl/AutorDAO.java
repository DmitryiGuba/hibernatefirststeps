package com.gubadev.simple.dao.impl;

import com.gubadev.simple.dao.DAO;
import com.gubadev.simple.entity.Autor;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class AutorDAO implements DAO<Autor> {
    private SessionFactory sessionFactory;

    @Override
    public List<Autor> findAll() {
        Session session = sessionFactory.openSession();
        List<Autor> autors = null;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            autors = session.createQuery("from Autor").list();
            tx.commit();
        } catch (HibernateException ex) {
            if(tx != null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return autors;
    }

    @Override
    public void add(Autor entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(entity);
            tx.commit();
        } catch (HibernateException ex){
            if(tx != null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public Autor get(int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        Autor autor = null;
        try {
            tx = session.beginTransaction();
            List<Autor> autors = session.createQuery("from Autor A where A.id=" + id).list();
            if(autors != null) autor = autors.get(0);
            tx.commit();
        } catch (HibernateException ex){
            if(tx != null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return autor;
    }

    @Override
    public void update(Autor entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(entity);
            tx.commit();
        } catch (HibernateException ex){
            if(tx != null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void delete(Autor entity) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(entity);
            tx.commit();
        } catch (HibernateException ex){
            if(tx != null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }


    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
