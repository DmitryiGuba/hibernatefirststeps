package com.gubadev.simple.dao;

import java.util.List;

public interface DAO<T> {

    List<T> findAll();
    void add(T entity);
    T get(int id);
    void update(T entity);
    void delete(T entity);



}
